
import java.awt.Color;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fran Lozano
 */
public class UsuarioCellRenderer extends ElemUsuario implements ListCellRenderer<Usuario> {

    @Override
    public Component getListCellRendererComponent(JList<? extends Usuario> list,
            Usuario value, int index, 
            boolean isSelected, boolean cellHasFocus) {
        
        //setters
        this.setOpaque(true);
        if(isSelected) {
            this.setBackground(Color.YELLOW);
        } else {
            this.setBackground(Color.WHITE);
        }
        return this;
    }
    
}
