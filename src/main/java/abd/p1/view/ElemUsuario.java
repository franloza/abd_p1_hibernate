package abd.p1.view;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */

public class ElemUsuario extends javax.swing.JPanel {

    //Components
    private AvatarPanel avatarPanel;
    private javax.swing.JLabel edadLabel;
    private javax.swing.JLabel nombreLabel;
    //Attributes
    private byte[] avatar;
    private String nombre;
    private int edad;


    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
        avatarPanel.setIcon(avatar);
    }

    public String getNombre() {
        return nombre;
    }

    public byte[] getAvatar() {
        return this.avatar;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
        nombreLabel.setText(nombre);
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
        edadLabel.setText((edad == 1) ? " 1 año" : edad + " años");
    }


    /**
     * Creates new form ElemUsuario
     */
    public ElemUsuario() {
        initComponents();
    }

    private void initComponents() {

        avatarPanel = new AvatarPanel();
        nombreLabel = new javax.swing.JLabel();
        edadLabel = new javax.swing.JLabel();

        nombreLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        nombreLabel.setText("Nombre");

        edadLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        edadLabel.setText("Edad");

        setLayout();
    }

    private void setLayout() {

        javax.swing.GroupLayout avatarPanelLayout = new javax.swing.GroupLayout(avatarPanel);
        avatarPanel.setLayout(avatarPanelLayout);
        avatarPanelLayout.setHorizontalGroup(
                avatarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 128, Short.MAX_VALUE)
        );
        avatarPanelLayout.setVerticalGroup(
                avatarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 128, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(avatarPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(nombreLabel)
                                        .addComponent(edadLabel))
                                .addContainerGap(245, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(avatarPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(nombreLabel)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(edadLabel)))
                                .addContainerGap(31, Short.MAX_VALUE))
        );
    }
}
