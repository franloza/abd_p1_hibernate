package abd.p1.view;

import abd.p1.controller.ProfileController;
import abd.p1.model.Usuario;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */

public class ProfileDialog extends javax.swing.JDialog {

    /*Componentes*/
    private javax.swing.JButton addAficionnButton;
    private javax.swing.JLabel aficionesLabel;
    private JList aficionesLista;
    private javax.swing.JScrollPane aficionesScroll;
    private javax.swing.JButton avatarButton;
    private javax.swing.JButton birthButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton delAficionButton;
    private javax.swing.JTextArea descripcionArea;
    private javax.swing.JLabel descripcionLabel;
    private javax.swing.JScrollPane descripcionScroll;
    private javax.swing.JLabel edadLabel;
    private javax.swing.JButton editAficionButton;
    private javax.swing.JButton interesButton;
    private javax.swing.JLabel interesLabel;
    private javax.swing.JLabel interesLabelInfo;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JLabel nombreLabel;
    private javax.swing.JButton nombreButton;
    private AvatarPanel avatarPanel;
    private javax.swing.JButton passButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JButton sexoButton;
    private javax.swing.JLabel sexoLabel;
    private javax.swing.JLabel sexoLabelInfo;
    private javax.swing.JFrame mainFrame;

    /*Atributos*/
    private ProfileController controller;
    private Usuario user;

    /*Constructor*/
    public ProfileDialog(JFrame mainFrame,Usuario user, ProfileController controller) {
        super(mainFrame,true);
        this.mainFrame = mainFrame;
        this.controller = controller;
        this.user = user;
        System.out.println("Iniciando ventana de perfil...");
        initComponents();
        setVisible(true);
    }

    private void initComponents() {

        /*Inicializar componentes*/
        mainPanel = new javax.swing.JPanel();
        descripcionLabel = new javax.swing.JLabel();
        descripcionScroll = new javax.swing.JScrollPane();
        descripcionArea = new javax.swing.JTextArea();
        avatarPanel = new AvatarPanel();
        aficionesLabel = new javax.swing.JLabel();
        aficionesScroll = new javax.swing.JScrollPane();
        aficionesLista = new javax.swing.JList<>();
        interesLabelInfo = new javax.swing.JLabel();
        interesLabel = new javax.swing.JLabel();
        birthButton = new javax.swing.JButton();
        sexoLabel = new javax.swing.JLabel();
        nombreButton = new javax.swing.JButton();
        nombreLabel = new javax.swing.JLabel();
        avatarButton = new javax.swing.JButton();
        edadLabel = new javax.swing.JLabel();
        sexoLabelInfo = new javax.swing.JLabel();
        addAficionnButton = new javax.swing.JButton();
        delAficionButton = new javax.swing.JButton();
        editAficionButton = new javax.swing.JButton();
        sexoButton = new javax.swing.JButton();
        interesButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        passButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();


        /*Configura el frame*/
        setTitle("Editar perfil");
        setResizable(false);
        setBackground(new java.awt.Color(220, 240, 240));
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        mainPanel.setBackground(new java.awt.Color(220, 240, 220));
        mainPanel.setBorder(new javax.swing.border.SoftBevelBorder(0));


        /*Configura componentes*/
        descripcionLabel.setText("Descripción: ");

        descripcionArea.setColumns(20);
        descripcionArea.setRows(5);
        String descripcion = user.getDescripcion();
        descripcionArea.setText(descripcion == null ? "Introduzca una descripción" : descripcion);
        descripcionScroll.setViewportView(descripcionArea);

        nombreLabel.setFont(new java.awt.Font("Tahoma", 1, 12));
        nombreLabel.setText(user.getNombre());

        aficionesLabel.setText("Aficiones:");

        List<String> aficiones = user.getAficiones();

        aficionesLista = new JList(aficiones==null?new String[1]:aficiones.toArray());
        aficionesLista.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        aficionesScroll.setViewportView(aficionesLista);

        interesLabel.setText("Busca:");
        String interes = user.getInteres();
        interesLabelInfo.setText((interes == null) ? "Indefinido" : interes);

        sexoLabel.setText("Sexo:");
        String genero = user.getGenero();
        sexoLabelInfo.setText((genero == null) ? "Indefinido" : genero);

        avatarPanel.setIcon(user.getAvatar());

        birthButton.setText("Cambiar fecha de nacimiento");
        birthButton.setActionCommand("");
        birthButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                birthButtonActionPerformed(evt);
            }
        });

        nombreButton.setText("Cambiar nombre");
        nombreButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreButtonActionPerformed(evt);
            }
        });

        avatarButton.setText("Cambiar avatar");
        avatarButton.setActionCommand("");
        avatarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                avatarButtonActionPerformed(evt);
            }
        });

        edadLabel.setText(user.getEdad() + " años");

        addAficionnButton.setText("Añadir afición");
        addAficionnButton.setActionCommand("");
        addAficionnButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAficionButtonActionPerformed(evt);
            }
        });

        delAficionButton.setText("Eliminar seleccionada");
        delAficionButton.setActionCommand("");
        delAficionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delAficionButtonActionPerformed(evt);
            }
        });

        editAficionButton.setText("Editar seleccionada");
        editAficionButton.setActionCommand("");
        editAficionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editAficionButtonActionPerformed(evt);
            }
        });

        sexoButton.setText("Cambiar sexo");
        sexoButton.setActionCommand("");
        sexoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sexoButtonActionPerformed(evt);
            }
        });

        interesButton.setText("Cambiar preferencia");
        interesButton.setActionCommand("");
        interesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                interesButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancelar");
        cancelButton.setActionCommand("");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        passButton.setText("Cambiar contraseña");
        passButton.setActionCommand("");
        passButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passButtonActionPerformed(evt);
            }
        });

        saveButton.setBackground(new java.awt.Color(158, 219, 251));
        saveButton.setText("Guardar cambios");
        saveButton.setActionCommand("");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                user.setDescripcion(descripcionArea.getText());
                saveButtonActionPerformed(evt);
            }
        });

        /*Cerrar conexion al cierre de la ventana*/
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                int i = JOptionPane.showConfirmDialog(null, "¿Quiere guardar los cambios?");
                    if(i==0 || i==1){
                        if (i==0) controller.save(user);
                        JDialog dialog = (JDialog) e.getSource();
                        dialog.dispose();
                    }
            }
        });

        setLayout();
        pack();
    }

    private void setLayout() {

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
                mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(mainPanelLayout.createSequentialGroup()
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(descripcionScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 619, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(mainPanelLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(mainPanelLayout.createSequentialGroup()
                                                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(mainPanelLayout.createSequentialGroup()
                                                                                .addGap(10, 10, 10)
                                                                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(aficionesLabel)
                                                                                        .addComponent(aficionesScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                        .addGroup(mainPanelLayout.createSequentialGroup()
                                                                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(interesLabel)
                                                                                        .addComponent(sexoLabel))
                                                                                .addGap(18, 18, 18)
                                                                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(interesLabelInfo)
                                                                                        .addComponent(sexoLabelInfo)))
                                                                        .addGroup(mainPanelLayout.createSequentialGroup()
                                                                                .addComponent(avatarPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addGap(18, 18, 18)
                                                                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(nombreLabel)
                                                                                        .addComponent(edadLabel))))
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                                .addComponent(addAficionnButton, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addComponent(delAficionButton, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addComponent(interesButton, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addComponent(sexoButton, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addComponent(editAficionButton, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addComponent(nombreButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGroup(mainPanelLayout.createSequentialGroup()
                                                                .addGap(0, 0, Short.MAX_VALUE)
                                                                .addComponent(descripcionLabel)
                                                                .addGap(358, 358, 358)
                                                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                        .addComponent(birthButton, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                                                        .addComponent(avatarButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                                .addContainerGap(18, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
                mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(mainPanelLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(addAficionnButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(delAficionButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(editAficionButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sexoButton)
                                .addGap(4, 4, 4)
                                .addComponent(interesButton)
                                .addGap(239, 239, 239))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(mainPanelLayout.createSequentialGroup()
                                                .addGap(16, 16, 16)
                                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(nombreLabel)
                                                        .addComponent(nombreButton))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(edadLabel)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(birthButton))
                                        .addGroup(mainPanelLayout.createSequentialGroup()
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(avatarPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(descripcionLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(avatarButton, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(descripcionScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(aficionesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(aficionesScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(sexoLabel)
                                        .addComponent(sexoLabelInfo))
                                .addGap(9, 9, 9)
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(interesLabelInfo)
                                        .addComponent(interesLabel))
                                .addGap(232, 232, 232))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cancelButton)
                                .addContainerGap())
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(20, 20, 20)
                                        .addComponent(passButton, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap(468, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 543, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cancelButton)
                                        .addComponent(saveButton))
                                .addGap(9, 9, 9))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addContainerGap(570, Short.MAX_VALUE)
                                        .addComponent(passButton)
                                        .addContainerGap()))
        );

        pack();
    }

    /*Triggers*/

    private void avatarButtonActionPerformed(java.awt.event.ActionEvent evt) {
        controller.updateAvatar(user);
        avatarPanel.setIcon(user.getAvatar());
    }

    private void birthButtonActionPerformed(java.awt.event.ActionEvent evt) {
        controller.updateEdad(mainFrame,user);
        edadLabel.setText((user.getEdad() + " años"));
    }

    private void addAficionButtonActionPerformed(java.awt.event.ActionEvent evt) {
        controller.addAficion(user);
        aficionesLista = new JList(user.getAficiones().toArray());
        aficionesScroll.setViewportView(aficionesLista);
    }

    private void delAficionButtonActionPerformed(java.awt.event.ActionEvent evt) {
        int index = this.aficionesLista.getSelectedIndex();
        controller.delAficion(user,index);
        aficionesLista = new JList(user.getAficiones().toArray());
        aficionesScroll.setViewportView(aficionesLista);
    }

    private void editAficionButtonActionPerformed(java.awt.event.ActionEvent evt) {
        int index = this.aficionesLista.getSelectedIndex();
        controller.editAficion(user,index);
        aficionesLista = new JList(user.getAficiones().toArray());
        aficionesScroll.setViewportView(aficionesLista);
    }

    private void sexoButtonActionPerformed(java.awt.event.ActionEvent evt) {
        controller.updateSexo(user);
        sexoLabelInfo.setText(user.getGenero());
    }

    private void interesButtonActionPerformed(java.awt.event.ActionEvent evt) {
        controller.updateInteres(user);
        interesLabelInfo.setText(user.getInteres());
    }

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
        dispose();
    }

    private void passButtonActionPerformed(java.awt.event.ActionEvent evt) {
        controller.updatePassword(user);
    }

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {
        controller.save(user);
        this.dispose();
    }

    private void nombreButtonActionPerformed(java.awt.event.ActionEvent evt) {
        controller.updateNombre(user);
        nombreLabel.setText(user.getNombre());
    }
}


