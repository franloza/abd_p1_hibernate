package abd.p1.view;
import javax.swing.*;
import java.awt.*;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */

public class AvatarPanel extends javax.swing.JPanel {

    private ImageIcon icon;
    private static final int SIZE = 128;
    private static final ImageIcon defaultIcon =
            new ImageIcon (AvatarPanel.class.getResource("/default-avatar.jpg"));

    public AvatarPanel() {
        this.setPreferredSize(new Dimension(SIZE, SIZE));
        icon = defaultIcon;

        //Rescale the image
        this.icon = new ImageIcon(this.icon.getImage().getScaledInstance (SIZE, SIZE, Image.SCALE_DEFAULT));
        this.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

    }

    public ImageIcon getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = (icon == null)?defaultIcon:new ImageIcon(icon);
        this.icon = new ImageIcon(this.icon.getImage().getScaledInstance (SIZE, SIZE, Image.SCALE_DEFAULT));
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(icon.getImage(),0,0,this);
    }
}