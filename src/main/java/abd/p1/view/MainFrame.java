package abd.p1.view;

import abd.p1.controller.MainController;
import abd.p1.model.Pregunta;
import abd.p1.model.Usuario;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
public class MainFrame extends javax.swing.JFrame {

    //Atributos
    private Usuario user;
    private MainController controller;
    private List <Usuario> usuarios;
    private List<Pregunta> preguntasRecomendadas;

    //Componentes
    private javax.swing.JCheckBox amigosBox;
    private javax.swing.JButton answerButton;
    private javax.swing.JLabel bestQuestionsLabel;
    private javax.swing.JButton editProfileButton;
    private javax.swing.JCheckBox filtroBox;
    private javax.swing.JTextField filtroField;
    private javax.swing.JTabbedPane mainTabPane;
    private javax.swing.JPanel preguntasPanel;
    private javax.swing.JList<Pregunta> questionsList;
    private javax.swing.JScrollPane questionsScroll;
    private javax.swing.JButton randomQuestionButton;
    private javax.swing.JList<Usuario> usuariosList;
    private javax.swing.JPanel usuariosPanel;
    private javax.swing.JScrollPane usuariosScroll;
    private javax.swing.JButton viewProfileButton;
    private javax.swing.JPanel mensajesPanel;
    private javax.swing.JScrollPane mensajesScroll;
    private javax.swing.JTextArea mensajesText;
    private javax.swing.JButton leidosButton;


    public MainFrame(Usuario user, MainController mainController) {
        this.user = user;
        this.controller = mainController;
        this.setVisible(true);
        System.out.println("Iniciando ventana principal...");
        initComponents();
    }

    private void initComponents() {

        mainTabPane = new javax.swing.JTabbedPane();
        usuariosPanel = new javax.swing.JPanel();
        usuariosScroll = new javax.swing.JScrollPane();
        usuariosList = new javax.swing.JList<>();
        filtroBox = new javax.swing.JCheckBox();
        amigosBox = new javax.swing.JCheckBox();
        editProfileButton = new javax.swing.JButton();
        viewProfileButton = new javax.swing.JButton();
        filtroField = new javax.swing.JTextField();
        preguntasPanel = new javax.swing.JPanel();
        bestQuestionsLabel = new javax.swing.JLabel();
        answerButton = new javax.swing.JButton();
        randomQuestionButton = new javax.swing.JButton();
        questionsScroll = new javax.swing.JScrollPane();
        questionsList = new javax.swing.JList<>();
        mensajesPanel = new javax.swing.JPanel();
        mensajesScroll = new javax.swing.JScrollPane();
        mensajesText = new javax.swing.JTextArea();
        leidosButton = new javax.swing.JButton();

        mainTabPane.addTab("Usuarios", usuariosPanel);
        mainTabPane.addTab("Preguntas", preguntasPanel);

        bestQuestionsLabel.setText("Mejor valoradas:");

        answerButton.setText("Responder");

        randomQuestionButton.setText("Pregunta aleatoria");

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        usuariosPanel.setPreferredSize(new Dimension(640, 640));
        setResizable(false);

        viewProfileButton.setEnabled(false);
        filtroBox.setEnabled(false);
        amigosBox.setEnabled(true);

        usuariosScroll.setViewportView(usuariosList);
        questionsScroll.setViewportView(questionsList);
        renderListUsuarios();
        renderListPreguntas();

        filtroBox.setText("Filtrar por nombre: ");

        amigosBox.setText("Mostrar sólo amigos");

        editProfileButton.setText("Modificar mi perfil");

        mensajesText.setColumns(20);
        mensajesText.setRows(5);
        mensajesScroll.setViewportView(mensajesText);

        leidosButton.setText("Marcar todos como leídos");
        leidosButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leidosButtonActionPerformed(evt);
            }
        });


        mainTabPane.addTab("Mensajes no leídos", mensajesPanel);


        editProfileButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                editProfileButtonActionPerformed(evt);
            }
        });

        viewProfileButton.setText("Ver perfil seleccionado");
        viewProfileButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                viewProfileButtonActionPerformed(evt);
            }
        });


        filtroField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent evt) {
                filtroFieldKeyTyped(evt);
            }
        });

        usuariosList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                viewProfileButton.setEnabled(true);
            }
        });

        answerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                answerButtonActionPerformed(evt);
            }
        });

        randomQuestionButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                randomButtonActionPerformed(evt);
            }
        });

        filtroBox.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
               renderListUsuarios();
            }
        });

        amigosBox.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                renderListUsuarios();
            }
        });
        setLayout();

        /*Cerrar conexion al cierre de la ventana*/
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e) {
                Object[] options = { "Si", "No" };
                int i = JOptionPane.showOptionDialog(null, "¿Seguro que quiere salir?", "Seleccionar una opción",
                        JOptionPane.DEFAULT_OPTION,JOptionPane.INFORMATION_MESSAGE, null,
                        options, options[1]);
                if(i==0){
                    controller.closeDB();
                    System.exit(0);//cierra aplicacion
                }
            }
        });
    }

    private void renderListUsuarios() {
        DefaultListModel<Usuario> model = new DefaultListModel<>();
        if (amigosBox.isSelected()) {
            filtroBox.setEnabled(true);
            this.usuarios = controller.getUsuariosByAmistad(user);
        }
        else {
            filtroBox.setEnabled(false);
            this.usuarios = controller.getUsuariosByInteres(user);
        }
        for (Usuario u : usuarios) model.addElement(u);
        usuariosList.setModel(model);
        usuariosList.setCellRenderer(new UsuarioCellRenderer());
    }

    private void renderListPreguntas() {
        DefaultListModel<Pregunta> model = new DefaultListModel<>();
        this.preguntasRecomendadas = controller.getPreguntasByRelevancia(user);
        for (Pregunta p : preguntasRecomendadas) model.addElement(p);
        questionsList.setModel(model);
        questionsList.setCellRenderer(new PreguntaCellRenderer());
    }
    
    
    private void setLayout() {
        javax.swing.GroupLayout usuariosPanelLayout = new javax.swing.GroupLayout(usuariosPanel);
        usuariosPanel.setLayout(usuariosPanelLayout);
        usuariosPanelLayout.setHorizontalGroup(
                usuariosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(usuariosPanelLayout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addGroup(usuariosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, usuariosPanelLayout.createSequentialGroup()
                                                .addGap(24, 24, 24)
                                                .addComponent(editProfileButton, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(31, 31, 31)
                                                .addComponent(viewProfileButton, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                                                .addGap(28, 28, 28))
                                        .addGroup(usuariosPanelLayout.createSequentialGroup()
                                                .addGroup(usuariosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(amigosBox)
                                                        .addGroup(usuariosPanelLayout.createSequentialGroup()
                                                                .addComponent(filtroBox)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(filtroField)))
                                                .addContainerGap())))
                        .addGroup(usuariosPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(usuariosScroll)
                                .addContainerGap())
        );
        usuariosPanelLayout.setVerticalGroup(
                usuariosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(usuariosPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(usuariosScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(usuariosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(filtroBox)
                                        .addComponent(filtroField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(amigosBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                                .addGroup(usuariosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(editProfileButton)
                                        .addComponent(viewProfileButton))
                                .addContainerGap())
        );

        javax.swing.GroupLayout preguntasPanelLayout = new javax.swing.GroupLayout(preguntasPanel);
        preguntasPanel.setLayout(preguntasPanelLayout);
        preguntasPanelLayout.setHorizontalGroup(
                preguntasPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(preguntasPanelLayout.createSequentialGroup()
                                .addGap(81, 81, 81)
                                .addComponent(answerButton, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 101, Short.MAX_VALUE)
                                .addComponent(randomQuestionButton, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(94, 94, 94))
                        .addGroup(preguntasPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(preguntasPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(questionsScroll)
                                        .addGroup(preguntasPanelLayout.createSequentialGroup()
                                                .addComponent(bestQuestionsLabel)
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        preguntasPanelLayout.setVerticalGroup(
                preguntasPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(preguntasPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(bestQuestionsLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(questionsScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addGroup(preguntasPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(answerButton)
                                        .addComponent(randomQuestionButton))
                                .addGap(34, 34, 34))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(mainTabPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(mainTabPane, javax.swing.GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout mensajesPanelLayout = new javax.swing.GroupLayout(mensajesPanel);
        mensajesPanel.setLayout(mensajesPanelLayout);
        mensajesPanelLayout.setHorizontalGroup(
                mensajesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(mensajesPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(mensajesScroll)
                                .addContainerGap())
                        .addGroup(mensajesPanelLayout.createSequentialGroup()
                                .addGap(143, 143, 143)
                                .addComponent(leidosButton, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(133, Short.MAX_VALUE))
        );
        mensajesPanelLayout.setVerticalGroup(
                mensajesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(mensajesPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(mensajesScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 423, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(leidosButton, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                                .addGap(18, 18, 18))
        );

        pack();
    }

    private void viewProfileButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Usuario selectedUser = usuariosList.getSelectedValue();
        if(user != null) controller.openViewProfileDialog(this,this.user,selectedUser);
    }

    private void editProfileButtonActionPerformed(java.awt.event.ActionEvent evt) {
        editProfile();
    }

	public void editProfile() {
		String interes = user.getInteres();
        controller.openProfileDialog(this,user);
        renderListUsuarios();
        user = controller.getUsuario(user);
	}

    private void filtroFieldKeyTyped(java.awt.event.KeyEvent evt) {
        if(filtroBox.isSelected()) {
            String filter = filtroField.getText().toLowerCase();
            DefaultListModel<Usuario> model = (DefaultListModel<Usuario>) usuariosList.getModel();
            for (Usuario u : usuarios) {
                if (!u.getNombre().toLowerCase().matches(".*" + filter + ".*")) {
                    if (model.contains(u)) {
                        model.removeElement(u);
                    }
                } else {
                    if (!model.contains(u)) {
                        model.addElement(u);
                    }
                }
            }
        }
    }

    private void answerButtonActionPerformed (java.awt.event.ActionEvent evt) {
        Pregunta selectedQuestion = questionsList.getSelectedValue();
        if(selectedQuestion != null)controller.openAnswerQuestionDialog(this, selectedQuestion);
        renderListPreguntas();
    }

    private void randomButtonActionPerformed (java.awt.event.ActionEvent evt) {
        Pregunta randomQuestion = controller.getRandomPregunta();
        if(randomQuestion != null)controller.openAnswerQuestionDialog(this, randomQuestion);
    }

    private void leidosButtonActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    public Usuario getUser() {
        return user;
    }

    public MainController getController() {
        return controller;
    }
}
