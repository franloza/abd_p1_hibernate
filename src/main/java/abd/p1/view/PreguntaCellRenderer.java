package abd.p1.view;

import abd.p1.model.Pregunta;
import javax.swing.*;
import java.awt.*;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
public class PreguntaCellRenderer extends ElemPregunta implements ListCellRenderer<Pregunta> {


    @Override
    public Component getListCellRendererComponent(JList<? extends Pregunta> list,
                                                  Pregunta value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {

        this.setEnunciado(value.getEnunciado());
        this.setOpciones(value.getOpciones().size());
        this.setOpaque(true);
        if(isSelected) {
            this.setBackground(Color.YELLOW);
        } else {
            this.setBackground(Color.WHITE);
        }

        return this;
    }
}
