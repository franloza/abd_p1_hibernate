package abd.p1.view;

import abd.p1.controller.ProfileController;
import abd.p1.model.Usuario;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Created by Fran Lozano on 24/04/2016.
 */

public class ViewProfileDialog extends javax.swing.JDialog {


    // Components              
	private javax.swing.JLabel aficionesLabel;
    private JList aficionesLista;
    private javax.swing.JScrollPane aficionesScroll;
    private AvatarPanel avatarPanel;
    private javax.swing.JList chatList;
    private javax.swing.JPanel chatPanel;
    private javax.swing.JScrollPane chatScroll;
    private javax.swing.JLabel compatibilidadEnunciadoLabel;
    private javax.swing.JLabel compatibilidadLabel;
    private javax.swing.JPanel compatibilidadPanel;
    private javax.swing.JTextArea descripcionArea;
    private javax.swing.JLabel descripcionLabel;
    private javax.swing.JScrollPane descripcionScroll;
    private javax.swing.JLabel distanciaLabel;
    private javax.swing.JLabel distanciaLabelInfo;
    private javax.swing.JLabel edadLabel;
    private javax.swing.JLabel enviarLabel;
    private javax.swing.JLabel interesLabel;
    private javax.swing.JLabel interesLabelInfo;
    private javax.swing.JLabel interesesLabel;
    private javax.swing.JList interesesList;
    private javax.swing.JScrollPane interesesScroll;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JTextField mensajeField;
    private javax.swing.JLabel nombreLabel;
    private javax.swing.JButton sendSolicitudButton;
    private javax.swing.JLabel sexoLabel;
    private javax.swing.JLabel sexoLabelInfo;
    private javax.swing.JTabbedPane viewProfileTabs; 
    private javax.swing.JFrame mainFrame;


    /*Atributos*/
    private ProfileController controller;
    private Usuario currentUser;
    private Usuario selectedUser;

    public ViewProfileDialog(JFrame mainFrame,Usuario currentUser, Usuario selectedUser, ProfileController controller) {
        super(mainFrame,true);
        this.mainFrame = mainFrame;
        this.controller = controller;
        this.currentUser = currentUser;
        this.selectedUser = selectedUser;
        System.out.println("Iniciando ventana de perfil...");
        initComponents();
        setVisible(true);
    }


    private void initComponents() {

    	sendSolicitudButton = new javax.swing.JButton();
        distanciaLabel = new javax.swing.JLabel();
        distanciaLabelInfo = new javax.swing.JLabel();
        viewProfileTabs = new javax.swing.JTabbedPane();
        mainPanel = new javax.swing.JPanel();
        descripcionLabel = new javax.swing.JLabel();
        descripcionScroll = new javax.swing.JScrollPane();
        descripcionArea = new javax.swing.JTextArea();
        nombreLabel = new javax.swing.JLabel();
        aficionesLabel = new javax.swing.JLabel();
        aficionesScroll = new javax.swing.JScrollPane();
        aficionesLista = new javax.swing.JList<String>();
        interesLabelInfo = new javax.swing.JLabel();
        interesLabel = new javax.swing.JLabel();
        sexoLabel = new javax.swing.JLabel();
        edadLabel = new javax.swing.JLabel();
        sexoLabelInfo = new javax.swing.JLabel();
        avatarPanel = new AvatarPanel();
        compatibilidadPanel = new javax.swing.JPanel();
        compatibilidadEnunciadoLabel = new javax.swing.JLabel();
        compatibilidadLabel = new javax.swing.JLabel();
        interesesLabel = new javax.swing.JLabel();
        interesesScroll = new javax.swing.JScrollPane();
        interesesList = new javax.swing.JList();
        chatPanel = new javax.swing.JPanel();
        chatScroll = new javax.swing.JScrollPane();
        chatList = new javax.swing.JList();
        enviarLabel = new javax.swing.JLabel();
        mensajeField = new javax.swing.JTextField();

        //Disable editable components

        this.setTitle("Perfil de " + this.selectedUser.getNombre());
        aficionesLista.setEnabled(false);
        descripcionArea.setForeground(Color.black);

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        mainPanel.setBackground(new java.awt.Color(220, 220, 250));
        mainPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        descripcionLabel.setText("Descripción: ");

        descripcionArea.setColumns(20);
        descripcionArea.setRows(5);
        descripcionScroll.setViewportView(descripcionArea);

        nombreLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nombreLabel.setText("<Nombre>");

        aficionesLabel.setText("Aficiones:");

        descripcionLabel.setText("Descripción: ");

        descripcionArea.setColumns(20);
        descripcionArea.setRows(5);
        descripcionArea.setEditable(false);
        String descripcion = this.selectedUser.getDescripcion();
        descripcionArea.setText(descripcion == null ? "Introduzca una descripción" : descripcion);
        descripcionScroll.setViewportView(descripcionArea);

        nombreLabel.setFont(new java.awt.Font("Tahoma", 1, 12));
        nombreLabel.setText(this.selectedUser.getNombre());

        aficionesLabel.setText("Aficiones:");

        List<String> aficiones = this.selectedUser.getAficiones();

        aficionesLista = new JList(aficiones==null?new String[1]:aficiones.toArray());
        aficionesLista.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        aficionesScroll.setViewportView(aficionesLista);

        interesLabel.setText("Busca:");
        String interes = this.selectedUser.getInteres();
        interesLabelInfo.setText((interes == null) ? "Indefinido" : interes);

        sexoLabel.setText("Sexo:");
        String genero = this.selectedUser.getGenero();
        sexoLabelInfo.setText((genero == null) ? "Indefinido" : genero);

        avatarPanel.setIcon(this.selectedUser.getAvatar());
        
        distanciaLabel.setText("Distancia:");

        distanciaLabelInfo.setText(((int)this.currentUser.getDistanceToUser(selectedUser)) + " m");
        
        viewProfileTabs.addTab("Perfil", mainPanel);

        compatibilidadEnunciadoLabel.setFont(new java.awt.Font("Tahoma", 1, 18));


        compatibilidadEnunciadoLabel.setText("Tu nivel de compatibilidad es de :");


        compatibilidadLabel.setFont(new java.awt.Font("Tahoma", 1, 24));
        //Conseguir nivel de compatibilidad
        int compatibilidad = controller.getCompatibilidad(currentUser,selectedUser);
        compatibilidadLabel.setText(compatibilidad + " %");

        interesesLabel.setFont(new java.awt.Font("Tahoma", 1, 18));
        interesesLabel.setText("Intereses comunes :");

        DefaultListModel<String> model = new DefaultListModel<>();
        List<String> interesesComunes = currentUser.getInteresesComunes(selectedUser);
        for (String i : interesesComunes) model.addElement(i);
        interesesList.setModel(model);

        interesesScroll.setViewportView(interesesList);
        
        viewProfileTabs.addTab("Compatibilidad", compatibilidadPanel);

        chatList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        chatScroll.setViewportView(chatList);

        enviarLabel.setText("Enviar mensaje:");
        viewProfileTabs.addTab("Chat", chatPanel);
        
        setLayout();

        sendSolicitudButton.setBackground(new java.awt.Color(158, 219, 251));
        sendSolicitudButton.setText("Envíar petición de amistad");
        sendSolicitudButton.setActionCommand("");
        sendSolicitudButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendSolicitudButtonActionPerformed(evt);
            }
        });

        if (currentUser.hasAmigo(selectedUser)) {
            sendSolicitudButton.setEnabled(false);}
        else {
            sendSolicitudButton.setEnabled(true);
        }
    }

    private void setLayout() {
    	javax.swing.GroupLayout avatarPanelLayout = new javax.swing.GroupLayout(avatarPanel);
        avatarPanel.setLayout(avatarPanelLayout);
        avatarPanelLayout.setHorizontalGroup(
            avatarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        avatarPanelLayout.setVerticalGroup(
            avatarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(aficionesLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(descripcionScroll, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(aficionesScroll)
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(avatarPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nombreLabel)
                                    .addComponent(edadLabel))
                                .addGap(0, 441, Short.MAX_VALUE)))
                        .addGap(18, 18, 18))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(descripcionLabel)
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addComponent(sexoLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(sexoLabelInfo))
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addComponent(interesLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(interesLabelInfo)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(nombreLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(edadLabel))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(avatarPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(9, 9, 9)
                .addComponent(descripcionLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(descripcionScroll, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(aficionesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(aficionesScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sexoLabel)
                    .addComponent(sexoLabelInfo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(interesLabel)
                    .addComponent(interesLabelInfo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout compatibilidadPanelLayout = new javax.swing.GroupLayout(compatibilidadPanel);
        compatibilidadPanel.setLayout(compatibilidadPanelLayout);
        compatibilidadPanelLayout.setHorizontalGroup(
            compatibilidadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(compatibilidadPanelLayout.createSequentialGroup()
                .addGroup(compatibilidadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(compatibilidadPanelLayout.createSequentialGroup()
                        .addGroup(compatibilidadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(compatibilidadPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(compatibilidadEnunciadoLabel))
                            .addGroup(compatibilidadPanelLayout.createSequentialGroup()
                                .addGap(317, 317, 317)
                                .addComponent(compatibilidadLabel))
                            .addGroup(compatibilidadPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(interesesLabel)))
                        .addGap(0, 309, Short.MAX_VALUE))
                    .addGroup(compatibilidadPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(interesesScroll)))
                .addContainerGap())
        );
        compatibilidadPanelLayout.setVerticalGroup(
            compatibilidadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(compatibilidadPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(compatibilidadEnunciadoLabel)
                .addGap(18, 18, 18)
                .addComponent(compatibilidadLabel)
                .addGap(18, 18, 18)
                .addComponent(interesesLabel)
                .addGap(18, 18, 18)
                .addComponent(interesesScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout chatPanelLayout = new javax.swing.GroupLayout(chatPanel);
        chatPanel.setLayout(chatPanelLayout);
        chatPanelLayout.setHorizontalGroup(
            chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chatPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chatScroll)
                    .addGroup(chatPanelLayout.createSequentialGroup()
                        .addComponent(enviarLabel)
                        .addGap(18, 18, 18)
                        .addComponent(mensajeField, javax.swing.GroupLayout.DEFAULT_SIZE, 565, Short.MAX_VALUE)))
                .addContainerGap())
        );
        chatPanelLayout.setVerticalGroup(
            chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chatPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(chatScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(enviarLabel)
                    .addComponent(mensajeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(distanciaLabel)
                .addGap(10, 10, 10)
                .addComponent(distanciaLabelInfo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(sendSolicitudButton, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(viewProfileTabs)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(viewProfileTabs, javax.swing.GroupLayout.PREFERRED_SIZE, 515, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(distanciaLabel)
                    .addComponent(distanciaLabelInfo)
                    .addComponent(sendSolicitudButton))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
    }

    private void sendSolicitudButtonActionPerformed(java.awt.event.ActionEvent evt) {
        controller.makeFriends(currentUser,selectedUser);
        sendSolicitudButton.setEnabled(false);
    }
}

