package abd.p1.view;

import com.toedter.calendar.JDateChooser;

import java.util.Date;

/**
 * Created by Fran Lozano on 22/04/2016.
 */
public class BirthDateDialog extends javax.swing.JDialog {

    /*Components*/
    private JDateChooser birthChooser;
    private javax.swing.JLabel birthLabel;
    private javax.swing.JButton aceptarButton;
    private javax.swing.JButton cancelarButton;

    /*Attributes*/
    private boolean accept;
    private  Date birthDate;

    /**
     * Creates new form BirthDateDialog
     */
    public BirthDateDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.accept = false;
        this.birthDate = new Date();
        this.setVisible(true);
    }


    private void initComponents() {

        birthLabel = new javax.swing.JLabel();
        aceptarButton = new javax.swing.JButton();
        cancelarButton = new javax.swing.JButton();
        birthChooser = new JDateChooser(new Date());

        this.setTitle("Escoja fecha de nacimiento");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        birthLabel.setText("Fecha de nacimiento: ");

        aceptarButton.setText("Aceptar");
        cancelarButton.setText("Cancelar");

        aceptarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                aceptarButtonMouseClicked(evt);
            }
        });

        cancelarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelarButtonMouseClicked(evt);
            }
        });

        setLayout();
    }

    private void setLayout() {
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(birthLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(birthChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
                        .addGroup(layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addComponent(aceptarButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                                .addComponent(cancelarButton)
                                .addGap(45, 45, 45))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(birthLabel)
                                        .addComponent(birthChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(aceptarButton)
                                        .addComponent(cancelarButton))
                                .addContainerGap())
        );

        pack();
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public boolean isAccepted() {
        return accept;
    }

    private void aceptarButtonMouseClicked(java.awt.event.MouseEvent evt) {
        this.birthDate = birthChooser.getDate();
        accept = true;
        this.dispose();
    }

    private void cancelarButtonMouseClicked(java.awt.event.MouseEvent evt) {
        this.dispose();
    }
}

