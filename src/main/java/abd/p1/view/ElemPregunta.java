package abd.p1.view;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */

public class ElemPregunta extends javax.swing.JPanel {

    //Components
    private javax.swing.JLabel enunciadoLabel;
    private javax.swing.JLabel opcionesLabel;

    //Attributes
    private String enunciado;
    private int opciones;


    public String getNombre() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
        enunciadoLabel.setText(enunciado);
    }

    public int getOpciones() {
        return opciones;
    }

    public void setOpciones(int opciones) {
        this.opciones = opciones;
        opcionesLabel.setText((opciones==1)?" 1 opción": opciones + " opciones");
    }


    public ElemPregunta() {
        initComponents();
    }

    private void initComponents() {

        enunciadoLabel = new javax.swing.JLabel();
        opcionesLabel = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(400, 76));
        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(400, 76));

        enunciadoLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        enunciadoLabel.setText("Pregunta");

        opcionesLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        opcionesLabel.setText("X opciones");

        setLayout();
    }

    private void setLayout() {
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(opcionesLabel)
                                                .addGap(0, 321, Short.MAX_VALUE))
                                        .addComponent(enunciadoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(enunciadoLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(opcionesLabel)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }
}