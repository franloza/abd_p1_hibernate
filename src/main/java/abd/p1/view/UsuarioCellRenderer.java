package abd.p1.view;
import abd.p1.model.Usuario;
import javax.swing.*;
import java.awt.*;


/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */

public class UsuarioCellRenderer extends ElemUsuario implements ListCellRenderer<Usuario> {


    @Override
    public Component getListCellRendererComponent(JList<? extends Usuario> list,
                                                  Usuario value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {

        this.setNombre(value.getNombre());
        this.setAvatar(value.getAvatar());
        this.setEdad(value.getEdad());
        this.setOpaque(true);
        if(isSelected) {
            this.setBackground(Color.YELLOW);
        } else {
            this.setBackground(Color.WHITE);
        }

        return this;
    }

}

