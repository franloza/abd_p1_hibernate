package abd.p1.controller;

import abd.p1.bd.DBFacade;
import abd.p1.exceptions.AuthException;
import abd.p1.model.Usuario;
import abd.p1.view.MainFrame;
import abd.p1.view.ProfileDialog;
import org.hibernate.exception.ConstraintViolationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
public class LoginController {

	private DBFacade facade;
	
	public LoginController(DBFacade facade) {
		this.facade = facade;
	}

	public void login(String email, char[] password) throws AuthException {
		Usuario user = new Usuario(email,new String(password));
		if(!facade.checkUsuario(user)) throw new AuthException("Email/Contraseña incorrecta");
        user = facade.getUsuario(email);
        facade.setCoordinates(user);
		new MainFrame(user,new MainController(facade));
    }

	public void register(String email, char[] password) throws AuthException {
		Usuario newUser = new Usuario(email,new String(password));

		/*Controles*/
		if(password.length< 8) {
			throw new AuthException("Contraseña debe tener al menos 8 caracteres");
		}

		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		if(!matcher.find())
			throw new AuthException("Introduzca un email válido");
		try {
			facade.registerUsuario(newUser);
		} catch (ConstraintViolationException ex) {
			throw new AuthException("Usuario existente. Escoja otro nombre por favor");
		}
	
		facade.setCoordinates(newUser);
		new MainFrame(newUser,new MainController(facade)).editProfile();
	}

	public void closeDB() {
		System.out.print("Cerrando conexion a BD...");
		facade.closeDB();
		System.out.println("OK");
	}

	/*Variables estáticas*/
	private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
		Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
}
