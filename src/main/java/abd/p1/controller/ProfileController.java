package abd.p1.controller;

import abd.p1.bd.DBFacade;
import abd.p1.model.Usuario;
import abd.p1.view.BirthDateDialog;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
public class ProfileController {

    private DBFacade facade;

    public ProfileController(DBFacade facade) {
        this.facade = facade;
    }

    public void updateNombre(Usuario user) {
        String newName = JOptionPane.showInputDialog("Introduce un nombre: ", user.getNombre());
        if (newName != null && !newName.trim().isEmpty()) {
            user.setNombre(newName);
        }
    }

    public void updatePassword(Usuario user) {
        JPasswordField pwd = new JPasswordField(15);
        int action = JOptionPane.showConfirmDialog(null, pwd, "Introduzca nueva contraseña", JOptionPane.OK_CANCEL_OPTION);
        if (action == 0) {
            user.setPassword(new String(pwd.getPassword()));
        }
    }

    public void updateAvatar(Usuario user) {
        JFileChooser fc = new JFileChooser();
        InputStream is = null;
        FileFilter imageFilter = new FileNameExtensionFilter(
                "Image files", ImageIO.getReaderFileSuffixes());
        fc.setFileFilter(imageFilter);

        try{
            if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                is = new FileInputStream(fc.getSelectedFile());
                System.out.println("Tamaño de la imagen: " + fc.getSelectedFile().length() + " bytes");
                if (fc.getSelectedFile().length() > 0) {
                    byte[] buffer = new byte[512*512];
                    is.read(buffer);
                    user.setAvatar(buffer);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateInteres(Usuario user) {
        JDialog.setDefaultLookAndFeelDecorated(true);
        Object[] selectionValues = { "Hombres", "Mujeres", "Ambos" };
        String initialSelection = "Hombres";
        Object selection = JOptionPane.showInputDialog(null, "¿En qué estás interesado?",
                "Seleccionar preferencia", JOptionPane.QUESTION_MESSAGE, null, selectionValues, initialSelection);
        if(selection != null) user.setInteres(selection.toString());
    }

    public void updateEdad(JFrame frame,Usuario user) {
        BirthDateDialog birthDialog = new BirthDateDialog(frame,true);
        if (birthDialog.isAccepted()) {
            user.setFechaNacimiento(birthDialog.getBirthDate());
        }
    }

    public void updateSexo(Usuario user) {
        JDialog.setDefaultLookAndFeelDecorated(true);
        Object[] selectionValues = { "Hombre", "Mujer"};
        String initialSelection = "Hombre";
        Object selection = JOptionPane.showInputDialog(null, "¿Cuál es tu género?",
                "Seleccionar género", JOptionPane.QUESTION_MESSAGE, null, selectionValues, initialSelection);
        if(selection != null) user.setGenero(selection.toString());
    }

    public void addAficion(Usuario user) {
        String newAficion = JOptionPane.showInputDialog("Introduce una afición: ", "");
        if (newAficion != null && !newAficion.trim().isEmpty()) {
            if(!user.getAficiones().contains(newAficion)) {
                user.getAficiones().add(newAficion);
            }
        }
    }

    public void delAficion(Usuario user, int index) {
        if (index >= 0 && user.getAficiones().get(index) != null) {
            user.getAficiones().remove(index);
        }

    }

    public void editAficion(Usuario user, int index) {
        if (index >= 0 && user.getAficiones().get(index) != null) {
            String newAficion = JOptionPane.showInputDialog("Edite la afición: ", user.getAficiones().get(index));
            if (newAficion != null && !newAficion.trim().isEmpty()) {
                if (!user.getAficiones().contains(newAficion)) {
                    user.getAficiones().set(index, newAficion);
                }
            }
        }
    }

    public void save(Usuario user) {

        try {
            facade.updateUsuario(user);
            
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public int getCompatibilidad(Usuario currentUser, Usuario selectedUser) {
        return facade.getCompatibilidad(currentUser,selectedUser);
    }

    public void makeFriends(Usuario currentUser, Usuario selectedUser) {
        currentUser.addAmigo(selectedUser);
        selectedUser.addAmigo(currentUser);
        facade.updateUsuario(currentUser);
        facade.updateUsuario(selectedUser);
    }
}


