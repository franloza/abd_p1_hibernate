package abd.p1.controller;

import abd.p1.bd.DBFacade;
import abd.p1.model.Opcion;
import abd.p1.model.Pregunta;
import abd.p1.model.Respuesta;
import abd.p1.model.Usuario;
import abd.p1.view.ContestarPreguntaDialog;
import abd.p1.view.MainFrame;
import abd.p1.view.ProfileDialog;
import abd.p1.view.ViewProfileDialog;

import java.util.List;

public class MainController {

    private DBFacade facade;

    public MainController(DBFacade facade) {
        this.facade = facade;
    }

    public void closeDB() {
        System.out.print("Cerrando conexion a BD...");
        System.out.println("OK");
    }

    public void openProfileDialog(MainFrame frame, Usuario user) {
        new ProfileDialog(frame, user, new ProfileController(facade));

    }

    public void openViewProfileDialog(MainFrame frame, Usuario currentUser, Usuario selectedUser) {
        new ViewProfileDialog(frame, currentUser, selectedUser, new ProfileController(facade));
    }

    public void openAnswerQuestionDialog(MainFrame frame, Pregunta selectedQuestion) {
        new ContestarPreguntaDialog(frame, true, selectedQuestion);
    }

    public List<Usuario> getUsuariosByInteres(Usuario currentUser) {
        return facade.getUsuariosByInteres(currentUser);
    }


    public List<Pregunta> getPreguntasByRelevancia(Usuario user) {
        return facade.getPreguntasByRelevancia();
    }

    public Usuario getUsuario(Usuario user) {
        return facade.getUsuario(user.getEmail());
    }


    public Pregunta getRandomPregunta() {
        return facade.getRandomPregunta();
    }


    public void createRespuesta(Opcion opcion, int relevancia,Usuario user) {
        Respuesta respuesta = new Respuesta(opcion.getPreguntaMadre(),opcion,user,relevancia);
        facade.createRespuesta(respuesta);
    }

    public List<Usuario> getUsuariosByAmistad(Usuario user, String filter) {
        return facade.getUsuariosByAmistad(user,filter);
    }

    public List<Usuario> getUsuariosByAmistad(Usuario user) {
        return facade.getUsuariosByAmistad(user);
    }
}