package abd.p1.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
@Entity
public class Invitacion extends Mensaje {

    @ManyToOne
	private Pregunta pregunta;

    //Default constructor
    public Invitacion() {
    	super();
	}

	public Invitacion(int idMensaje, Date timestamp, boolean leido, Usuario origen, Usuario destino, Pregunta pregunta) {
        super(idMensaje, timestamp, leido, origen, destino);
        this.pregunta = pregunta;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }
}
