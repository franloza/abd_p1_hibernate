package abd.p1.model;


import java.io.Serializable;

public class RespuestaPK implements Serializable {
    private Usuario usuario;
    private Pregunta pregunta;

    public RespuestaPK(Usuario usuario, Pregunta pregunta) {
        this.usuario = usuario;
        this.pregunta = pregunta;
    }

    public RespuestaPK( ) {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }
}
