package abd.p1.model;

import java.util.Date;

import javax.persistence.*;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name="Mensajes")
public class Mensaje {

	@Id
	@Column(name="ID_Mensaje")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int idMensaje;
	
	@Column (nullable=false)
    private Date timestamp;
	
	@Column (nullable=false)
    private boolean leido;
	
	@ManyToOne 
    private Usuario origen;
	
	@ManyToOne
    private Usuario destino;


    public Mensaje(int idMensaje, Date timestamp, boolean leido, Usuario origen, Usuario destino) {
        this.idMensaje = idMensaje;
        this.timestamp = timestamp;
        this.leido = leido;
        this.origen = origen;
        this.destino = destino;
    }  
    
    //Default constructor
    public Mensaje() {

	}



	public Usuario getOrigen() {
        return origen;
    }

    public void setOrigen(Usuario origen) {
        this.origen = origen;
    }

    public Usuario getDestino() {
        return destino;
    }

    public void setDestino(Usuario destino) {
        this.destino = destino;
    }

    public int getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(int idMensaje) {
        this.idMensaje = idMensaje;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isLeido() {
        return leido;
    }

    public void setLeido(boolean leido) {
        this.leido = leido;
    }
}

