package abd.p1.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
@Entity
public class Solicitud extends Mensaje {

	public Solicitud() {}
	
    @Column (length=500)
    private String texto;

    public Solicitud(int idMensaje, Date timestamp, boolean leido, Usuario origen, Usuario destino,String texto) {
        super(idMensaje, timestamp, leido, origen, destino);
        this.texto = texto;
    }
}
