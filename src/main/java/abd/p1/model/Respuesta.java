package abd.p1.model;

import javax.persistence.*;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */

@Entity
@IdClass(RespuestaPK.class)
@Table(name="Respuestas")
public class Respuesta {

    @Id
    @ManyToOne
    private Usuario usuario;

    @Id
    @ManyToOne
    private Pregunta pregunta;

    @ManyToOne
    private Opcion opcion;

    @Column
    private int relevancia;

    public Respuesta() {}
    
    public Respuesta(Pregunta pregunta, Opcion opcion, Usuario usuario, int relevancia) {
        this.opcion = opcion;
        this.relevancia = relevancia;
        this.usuario = usuario;
        this.pregunta = pregunta;
    }

    public Opcion getOpcion() {
        return opcion;
    }

    public void setOpcion(Opcion opcion) {
        this.opcion = opcion;
    }

    public int getRelevancia() {
        return relevancia;
    }

    public void setRelevancia(int relevancia) {
        this.relevancia = relevancia;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    public Usuario getUsuario() {
        return usuario;
    }
}
