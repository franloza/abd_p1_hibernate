package abd.p1.model;

import javax.persistence.*;
import java.util.*;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
@Entity
@Table(name="Usuarios")
public class Usuario {
	@Id
	@Column(nullable=false,length=50)
    private String email;

	@Column(nullable=false,length=20)
    private String password;
	
	@Column(nullable=false,length=50)
    private String nombre;
	
	@Column(length=10)
    private String genero;
	
	@Column(length=10)
    private String interes;
	
	@Column
    private float latitud;
	
	@Column
    private float longitud;
	
	@Column
    private Date fechaNacimiento;
	
	@Column
	@Lob
    private byte[] avatar;
	
	@Column(length=500)
    private String descripcion;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> aficiones;
	
	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<Usuario> amigos;

    /*Default Constructor*/
    public Usuario () {
    	
    }
      
    public Usuario(String email, String password) {
		this.email = email;
		this.password = password;
        this.nombre = "Nuevo Usuario";
        this.genero = "Indefinido";
        this.interes = "Indefinido";
        this.fechaNacimiento = new Date();
        updateCoordinates();
        this.amigos = new ArrayList<Usuario>();
        this.aficiones = new ArrayList<String>();
	}


	public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getInteres() {
        return interes;
    }

    public void setInteres(String interes) {
        this.interes = interes;
    }

    public float getLatitud() {
        return latitud;
    }


    public float getLongitud() {
        return longitud;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<String> getAficiones() {
        return aficiones;
    }

    public List<Usuario> getAmigos() {
        return this.amigos;
    }

    public void addAmigo(Usuario user) {
        if(!this.amigos.contains(user)) {
            this.amigos.add(user);
        }

    }

    public boolean hasAmigo(Usuario selectedUser) {
        for(Usuario u:amigos) {
            if(u.getEmail().equals(selectedUser.getEmail())) return true;
        }
        return false;
    }

    public int getEdad () {
        Calendar dob = Calendar.getInstance();
        dob.setTime(this.fechaNacimiento);
        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR))
            age--;
        return age;
    }
    
    private float nextFloat(float min, float max)
    {
        Random random = new Random();
        return min + random.nextFloat() * (max - min);
    }
    
    public void updateCoordinates () {
        this.latitud = (float)(nextFloat(rangeLat[0],rangeLat[1]) * Math.PI) / 180;
        this.longitud = (float) (nextFloat(rangeLon[0],rangeLon[1]) * Math.PI) / 180;
    }
    
    public double getDistanceToUser (Usuario target) {
    	final int EARTHRADIUS = 6371000;
    	float incLatitud = this.getLatitud() - target.getLatitud();
    	float incLongitud = this.getLongitud() - target.getLongitud();
    	double a = Math.pow(Math.sin(incLatitud/2), 2) + Math.cos(this.getLatitud())
    			* Math.cos(target.getLatitud()) * Math.pow(Math.sin(incLongitud/2), 2);
    	double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    	return EARTHRADIUS * c; 	
    }

    public List<String> getInteresesComunes(Usuario userToCompare) {
        List<String> interesesComunes = new LinkedList<>();
        List <String> list1 = this.getAficiones();
        List <String> list2 = userToCompare.getAficiones();

        for (String s : list1) {
            if(list2.contains(s)) {
                interesesComunes.add(s);
            }
        }

        return interesesComunes;
    }

    //Static variables
    
   private static float rangeLat [] = {40f,41.2f};
   private static float rangeLon [] = {3f,4.5f};

}
