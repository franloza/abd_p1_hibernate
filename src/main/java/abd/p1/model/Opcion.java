package abd.p1.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
@Entity
@Table(name="Opciones")
public class Opcion implements Serializable{
	
	@Id
	@Column(name="ID_Respuesta")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idRespuesta;
	
	@Column(nullable = false, length=2)
	private int numeroOrden;
	
	@Column(nullable = false, length=500)
	private String texto;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Pregunta preguntaMadre;
	
	//Default constructor
	public Opcion() {
		
	}

	public Pregunta getPreguntaMadre() {
		return preguntaMadre;
	}

	public void setPreguntaMadre(Pregunta preguntaMadre) {
		this.preguntaMadre = preguntaMadre;
	}

	public int getNumeroOrden() {
		return numeroOrden;
	}

	public void setNumeroOrden(int numeroOrden) {
		this.numeroOrden = numeroOrden;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public int getIdRespuesta() {
		return idRespuesta;
	}

	public void setIdRespuesta(int idRespuesta) {
		this.idRespuesta = idRespuesta;
	}

}
