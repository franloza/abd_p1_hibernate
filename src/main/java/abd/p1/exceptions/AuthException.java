package abd.p1.exceptions;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
public class AuthException extends RuntimeException {
    public AuthException (String msg){
        super(msg);
    }


}
