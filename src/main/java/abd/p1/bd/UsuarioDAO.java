package abd.p1.bd;

import abd.p1.model.Usuario;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class UsuarioDAO {

	private SessionFactory sf;

	public UsuarioDAO(SessionFactory sf) {
		this.sf =sf;
	}

	public boolean checkUsuario(Usuario user) {
        Session session = sf.openSession();
        Query query = session.createQuery("FROM Usuario AS u WHERE u.email = :email AND u.password= :password");
        query.setString("email", user.getEmail());
        query.setString("password", user.getPassword());
        Object o = query.uniqueResult();
        session.close();
        return o != null;
	}

    public void nuevoUsuario(Usuario user)  {
        Session session = sf.openSession();
        System.out.println("Creando nuevo usuario: " + user.getEmail());
        Transaction tr = session.beginTransaction();
        session.save(user);
        tr.commit();
        session.close();
    }

    public void updateUsuario (Usuario user) {
        Session session = sf.openSession();
        System.out.println("Actualizando usuario: " + user.getEmail());
        Transaction tr = session.beginTransaction();
        Usuario userTmp = getUsuario(user.getEmail());
        userTmp.getAficiones().clear();
        session.saveOrUpdate(user);
        tr.commit();
        session.close();
    }

    public Usuario getUsuario(String email) {
        Session session = sf.openSession();
        System.out.println("Cargando usuario: " + email);
        Usuario user = session.get(Usuario.class, email);
        //Actualiza coordenadas automáticamente en cada inicio de sesión
        session.close();
        return user;
    }

    public List<Usuario> getAll(Usuario currentUser) {
        Session session = sf.openSession();
        Query query = session.createQuery("FROM Usuario AS u WHERE u.email <> :email" );
        query.setString("email",currentUser.getEmail());
        List<Usuario> users = query.list();
        session.close();
        return users;
    }

    public List<Usuario> getAllByInteres(Usuario currentUser) {
        Session session = sf.openSession();
        Query query;
        String queryStr ="FROM Usuario AS u WHERE u.email <> :email";
        if(!currentUser.getInteres().equals("Ambos") ) {
            queryStr = queryStr.concat(" AND u.genero=:genero");
            String genero;
            switch(currentUser.getInteres()) {
                case "Hombres":{genero="Hombre";break;}
                case "Mujeres":{genero="Mujer";break;}
                default:genero="Indefinido";
            }
            queryStr = queryStr.concat(" ORDER BY ((u.latitud - :ulatitud)*(u.latitud - :ulatitud)+(u.longitud - :ulongitud)*(u.longitud - :ulongitud))");
            query = session.createQuery(queryStr);
            query.setString("genero",genero);
        }
        else {
        	queryStr = queryStr.concat(" ORDER BY ((u.latitud - :ulatitud)*(u.latitud - :ulatitud)+(u.longitud - :ulongitud)*(u.longitud - :ulongitud))");
            query = session.createQuery(queryStr);

        }
        query.setString("email",currentUser.getEmail());
        query.setFloat("ulatitud", currentUser.getLatitud());
        query.setFloat("ulongitud", currentUser.getLongitud());
        query.setMaxResults(20);
        List<Usuario> users = query.list();
        session.close();
        return users;
    }

	public void setCoordinates(Usuario user) {
        user.updateCoordinates();
        updateUsuario(user);
	}

    public int getCompatibilidad(Usuario user1, Usuario user2) {
        Session session = sf.openSession();
        int compatibilidad = 0;

        //Mtotal Query

        // SQL Query:
        //      SELECT SUM(relevancia) FROM respuestas WHERE usuario_email = :email1 OR usuario_email = :email2
        //      GROUP BY pregunta_ID_Pregunta HAVING COUNT(pregunta_ID_Pregunta) = 2;

        String queryStr ="SELECT SUM(r.relevancia) FROM Respuesta r WHERE r.usuario.email = :email1 OR r.usuario.email = :email2 " +
                "GROUP BY r.pregunta.id HAVING COUNT(r.pregunta.id) = 2";
        Query query = session.createQuery(queryStr);
        query.setString("email1",user1.getEmail());
        query.setString("email2",user2.getEmail());
        List<Object[]> result = (List<Object[]>) query.list();
        long Mtotal = 0;
        for(Object o:result){
            Mtotal += (long)o;
        }

        //Macierto Query

        // SQL Query:
        //      SSELECT SUM(relevancia) FROM respuestas WHERE usuario_email = :email1 OR usuario_email = :email2
        //      GROUP BY opcion_ID_Respuesta HAVING COUNT(opcion_ID_Respuesta) = 2;
        queryStr ="SELECT SUM(r.relevancia) FROM Respuesta r WHERE r.usuario.email = :email1 OR r.usuario.email = :email2 " +
                "GROUP BY r.opcion.id HAVING COUNT(r.opcion.id) = 2";
        query = session.createQuery(queryStr);
        query.setString("email1",user1.getEmail());
        query.setString("email2",user2.getEmail());
         result = (List<Object[]>) query.list();
        long Macierto = 0;
        for(Object o:result){
            Macierto += (long)o;
        }

        if (Mtotal != 0){
            compatibilidad = (int) Math.round((((double)Macierto) / Mtotal) *100);
        }

        session.close();
        return compatibilidad;
    }

    public List<Usuario> getAllByAmistad(Usuario user, String filter) {
        Session session = sf.openSession();
        String filterClause = (filter!=null)?" AND u.amigos.nombre LIKE :filter":"";
        String queryStr ="SELECT u.amigos FROM Usuario AS u WHERE u.email = :email" + filterClause;
        Query query = session.createQuery(queryStr);
        query.setString("email",user.getEmail());
        if (filter!=null) query.setString("filter",filter);
        List<Usuario> users = query.list();
        session.close();
        return users;
    }
}
