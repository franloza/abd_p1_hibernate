package abd.p1.bd;

import abd.p1.model.Opcion;
import abd.p1.model.Pregunta;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
class PreguntaDAO {

    private SessionFactory sf;

    public PreguntaDAO(SessionFactory sf) {
        this.sf =sf;
    }

    public List<Pregunta> getAll() {
        //TEST!
        //createTestQuestion();

        Session session =sf.openSession();
        Query query = session.createQuery("FROM Pregunta" );
        List<Pregunta> preguntas = query.list();
        session.close();
        return preguntas;
    }

    /*En esta lista deben mostrarse las veinte preguntas de la BD con mejor valoración
    media (en orden decreciente). Entendemos como ‘valoración media’ de una pregunta la suma
    de todas las valoraciones recibidas por todos los usuarios que han contestado a esa pregunta dividida
    entre el número de usuarios que han contestado a dicha pregunta. Utiliza la función de
    agregación AVG de HQL.*/
    public List<Pregunta> getPreguntasByRelevancia() {
        Session session =sf.openSession();
        //Query query = session.createQuery("SELECT p FROM Respuesta r RIGHT JOIN r.opcion AS o JOIN o.preguntaMadre AS p " +
                //"GROUP BY p ORDER BY AVG(r.relevancia) DESC");
        Query query = session.createQuery("SELECT p FROM Respuesta r RIGHT JOIN r.pregunta p " +
                "GROUP BY p ORDER BY AVG(r.relevancia) DESC");
        query.setMaxResults(20);
        List<Pregunta> preguntas = query.list();
        session.close();
        return preguntas;
    }


    private void createTestQuestion () {
        Session session =sf.openSession();
        Pregunta p = new Pregunta();
        p.setEnunciado("¿Crees en Dios?");
        Opcion o1 = new Opcion();
        o1.setNumeroOrden(1);
        o1.setPreguntaMadre(p);
        o1.setTexto("Si");
        p.addOpcion(o1);
        Opcion o2 = new Opcion();
        o2.setNumeroOrden(2);
        o2.setPreguntaMadre(p);
        o2.setTexto("No");
        p.addOpcion(o2);
        Transaction tr = session.beginTransaction();
        session.save(p);
        session.save(o1);
        session.save(o2);
        tr.commit();
        session.close();
    }

    public Pregunta getRandom() {
        Session session =sf.openSession();
        Query query = session.createQuery("FROM Pregunta ORDER BY RAND()")
                             .setMaxResults(1);
        Pregunta p = (Pregunta) query.uniqueResult();
        session.close();
        return p;
    }
}
