package abd.p1.bd;

import abd.p1.model.Pregunta;
import abd.p1.model.Respuesta;
import abd.p1.model.Usuario;
import org.hibernate.SessionFactory;

import java.util.List;
/**
 * Created by Fran Lozano  y Jose Miguel Maldonado.
 */
public class DBFacade {
	
	//Attributes
    private SessionFactory sf;
	private UsuarioDAO uDao;
    private PreguntaDAO pDao;
    private RespuestaDAO rDao;
	
	public DBFacade(SessionFactory sf) {
        this.sf = sf;
		this.uDao = new UsuarioDAO(sf);
        this.pDao = new PreguntaDAO(sf);
        this.rDao = new RespuestaDAO(sf);
	}

	public boolean checkUsuario(Usuario user) {
        return uDao.checkUsuario (user);
	}

	public void registerUsuario(Usuario newUser) {
        uDao.nuevoUsuario(newUser);
    }

    public void updateUsuario(Usuario user) {
        uDao.updateUsuario(user);
    }

    public void closeDB() {
        sf.close();
    }

    public Usuario getUsuario(String email) {
        return uDao.getUsuario(email);
    }

    public List<Usuario> getAllUsuarios(Usuario currentUser) {
        return uDao.getAll(currentUser);
    }

    public List<Usuario> getUsuariosByInteres(Usuario currentUser) {
        return uDao.getAllByInteres(currentUser);
    }

    public List<Pregunta> getAllPreguntas () {return pDao.getAll();}

	public void setCoordinates(Usuario user) {
		uDao.setCoordinates(user);	
	}

    public Pregunta getRandomPregunta() {
        return pDao.getRandom();
    }

    public void createRespuesta(Respuesta respuesta) {
        rDao.nuevaRespuesta(respuesta);
    }

    public List<Pregunta> getPreguntasByRelevancia() {
        return pDao.getPreguntasByRelevancia();
    }

    public int getCompatibilidad(Usuario user1, Usuario user2) {
        return uDao.getCompatibilidad(user1,user2);
    }

    public List<Usuario> getUsuariosByAmistad(Usuario user, String filter) {
        return uDao.getAllByAmistad(user,filter);
    }

    public List<Usuario> getUsuariosByAmistad(Usuario user) {
        return uDao.getAllByAmistad(user,null);
    }
}
