package abd.p1.bd;

import abd.p1.model.Respuesta;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class RespuestaDAO {

    private SessionFactory sf;

    public RespuestaDAO(SessionFactory sf) {
        this.sf =sf;
    }

    public void nuevaRespuesta(Respuesta respuesta)  {
        Session session = sf.openSession();
        System.out.println("Creando nueva respuesta para " + respuesta.getUsuario().getEmail());
        Transaction tr = session.beginTransaction();
        session.saveOrUpdate(respuesta);
        tr.commit();
        session.close();
    }
}
